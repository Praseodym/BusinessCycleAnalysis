# Business Cycle Analysis

Business cycle analysis is the study of the fluctuations in economic activity that an economy experiences over time. It involves identifying and evaluating the phases of expansion and contraction in the economy.

Business cycle analysis is important for understanding the current state of the economy and making informed decisions about investments, monetary policy, and fiscal policy.

## In this Repository
We take a brief look at the current state of the US and German economy by using two leading indicators which are less known to the puplic.
